function NMFL_0experiment()
p.d = 100;
p.N = 10;
K = [200,400,800];
L = [1,10:10:50];

for iter = 1:5
    for k = 1:length(K)
        W{iter,k} = NormalizeMatrix(abs(randn(p.d,K(k))));
        for l = 1:length(L)
            H1 = zeros(K(k),p.N);
            for n = 1:p.N
                Is = randperm(K(k),L(l));
                H1(Is,n) = abs(randn(L(l),1));
            end
            H{iter,k,l} = NormalizeMatrix(H1);
            X{iter,k,l} = (W{iter,k}*H{iter,k,l});
        end
    end
end

save('nmf_experiment_data.mat');
end

