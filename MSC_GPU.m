function H = MSC_GPU(X,W,H,p)

[~,N] = size(X);
[~,K] = size(W);

lambda = gpuArray(repmat(p.lambda,K,N));
H = gpuArray(H);
X = gpuArray(X);
W = gpuArray(W);
WtW = gpuArray(W'*W);
WtX = gpuArray(W'*X);
EPS = gpuArray(eps*ones(size(H)));

switch p.sparseType
    case 'rl2'
        T = gpuArray(repmat(p.tau,K,N));
        for iter = 1:50
            Hprev = H;
            for ii = 1:10
                Q =  lambda.*(1+T)./(T+H.^2);
                for kk = 1:2e3
                    H = H.*(WtX)./(WtW*H+Q.*H)+EPS;
                end
            end
            tmp = abs((sqrt(sum(Hprev.^2,1))-sqrt(sum(H.^2,1)))./sqrt(sum(Hprev.^2,1)));
            T(:,tmp < sqrt(T(1,:))/100) = T(:,tmp < sqrt(T(1,:))/100)/10;
            if(max(T(:)) < 1e-4)
                break;
            end
        end
    case 'rl1'
        for iter = 1:500
            %             Hprev = H;
            %             for ii = 1:10
            Q = lambda./(T+H);
            for kk = 1:2e3
                H = H.*(WtX)./(WtW*H+Q+EPS)+EPS;
                %                 H(H < eps) = eps;
            end
            %             hold off; plot(p.H(:,1)); hold all; plot(H(:,1)); drawnow;
            %             end
            % %             if(max(sqrt(sum(abs(Hprev-H).^2,1))./sqrt(sum(abs(Hprev).^2,1))) < 1e-6)
            %                 break;
            %             end
        end
    case 'l1'
        for iter = 1:500*2e3
            H = H.*(WtX)./(WtW*H+lambda+EPS)+EPS;
        end
end

H = gather(H);
end