function [Is,errors] = OMP(A,b,thresh)

[n,m] = size(A);

%Initialize
x = zeros(m,1);
bp = b - A*x;

p = 1; errors(p,1) = thresh+1;

while( errors(p,1) > thresh )
    %Step 1
    if(exist('tmp','var'))
        %mn flops
        tmp = tmp - (A.'*qkp)*(qkp.'*b);
        
        %n flops
        kp = repmat(norm(bp,2)^2,m,1)-abs(tmp).^2;
        [~,kp] = min(kp);
    else
        tmp = A.'*bp;
        kp = repmat(norm(bp,2)^2,m,1)-abs(tmp).^2;
        [~,kp] = min(kp);
    end
    
    
    %Step 2
    if(exist('Qp','var'))
        %ns^2+n^2
        qkp = (eye(n)-Qp*Qp.')*A(:,kp);
        qkp = qkp/norm(qkp,2);
        
        Qp = [Qp qkp];
        
        Psp = Psp + qkp*qkp.';
    else
        qkp = A(:,kp);
        Qp = A(:,kp);
        
        Psp = qkp*qkp.';
    end
    
    %n flops
    bp = bp- qkp*(qkp.'*b);
    
    if(exist('Is','var'))
        Is = union(Is,kp);
    else
        Is = kp;
    end
    
    p = p+1;
    errors(p,1) = norm(bp,2);
end

end