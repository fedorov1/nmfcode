function H = NMFGPU(X,W,H,p)

H = gpuArray(H);
X = gpuArray(X);
W = gpuArray(W);
WtW = gpuArray(single(W'*W));

for kk = 1:2e3
    WtX = W'*X; WtWH = WtW*H;
    H = updateRule(WtX,WtWH,H);
end
H = gather(H);

end

function H = updateRule(WtX,WtWH,H)
H = H.*((WtX)./(WtWH+eps));
end