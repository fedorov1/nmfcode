function [x] = Reweightedl1NNSBL(A,b,s,p)
% Non-negative Sparse Bayesian learning with
% Rectified Gaussian Mixtures
%
% Estimation Method: Mode Point Estimate for hat(x) is the'location' mu
%
% Author: Alican Nalci - DSP Lab University of California, San Diego
% This code is a non-negative RG extension to SBL algorithm
% published in
% [1] "An Empirical Bayesian Strategy for Solving the Simultaneous
%      Sparse Approximation Problem", D. Wipf, B. Rao
%
[~, dim2]   = size(A);
epsilon     = 1e-6;
lambda      = p.sigmaSquared;
gamma       = ones(dim2,1);
mu_new      = zeros(dim2,1);
x_hat = zeros(dim2,1);
diff        = inf;
iter = 1;
while(diff >epsilon && iter < 1000)
    gamma_old = gamma;
    Gamma      = diag(gamma);
    sqrtGamma  = diag(sqrt(gamma));
    [U,S,V]    = svd(A*sqrtGamma,'econ');
    diagEigen  = diag(S);
    Ainv       = sqrtGamma*V*diag((diagEigen./(diagEigen.^2+lambda+eps)))*U';
    mu_new     = Ainv*b;
    sigma      = real(gamma-(sum(Ainv'.*(A*Gamma)))');
    %     Sigma      = diag(sigma);
    %     L          = real(chol(Sigma,'lower'));
    %     beta       = L\mu_new;
    %     %Calculate hat(x) and Difference
    %     E_w = beta+sqrt(2/pi)*exp(-(beta.^2)/2)./(erfc(-beta./sqrt(2))+eps);
    %     x_hat = L*E_w;
    %     diff = norm(x_hat-x_hat_old,2)/norm(x_hat_old);
    %     %calculate R and gamma
    %     R         = E_w*E_w';
    %     diag_R    = 1+beta.^2+sqrt(1/pi)*beta.*exp(-(beta.^2)./2)./...
    %         (erfc(-beta./sqrt(2))+eps);
    %     R(eye(size(R))~=0) = diag_R;
    %
    tau = (-100+sqrt(100^2+4*(100-1+2*dim2)*sum(gamma)))/(2*sum(gamma));
    %     E_h2 = sum(L.*R*L,2);
    E_h2 = mu_new.^2+sigma+mu_new.*sqrt(2/pi).*sqrt(sigma).*erfc(-mu_new.^2./(2*sigma))./erfc(-mu_new./(sqrt(2*sigma)+eps));
    
    gamma = (-1+sqrt(1+4*tau^2*E_h2))/(2*tau^2);
    
    %     R = L'*R*L;
    
    iter = iter + 1;
    diff = norm(gamma-gamma_old,2)/norm(gamma_old);
end

x = lsqnonneg([A;diag(1./sqrt(gamma))*p.sigmaSquared],[b;zeros(length(gamma),1)]);
[tmp,ind] = sort(abs(x),'descend');
Is_hat = ind(1:s);
x = zeros(size(x));
x(Is_hat) = tmp(1:s);
end
