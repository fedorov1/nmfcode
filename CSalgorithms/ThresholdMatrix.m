function Hout = ThresholdMatrix(H,L)

Hout = zeros(size(H));

for n = 1:size(H,2)
   [~,I] = sort(H(:,n),'descend');
   Hout(I(1:L),n) = H(I(1:L),n);
end

end