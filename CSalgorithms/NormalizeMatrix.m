function A = NormalizeMatrix(A)
A = A*diag(1./sqrt(sum(A.^2,1)));
end