function H = NNBOMP(X,W,p)

R = X;
groups = unique(p.groups);
usedGroups = [];

for ii = 1:p.s
    unusedGroups = setdiff(groups,usedGroups);
    clear d tmp
    for g = 1:length(unusedGroups)
       [tmp(:,g),d(g)] = lsqnonneg(W(:,ismember(p.groups,union(usedGroups,unusedGroups(g)))),X); 
    end
    [~,ind] = min(d);
    
    usedGroups = union(usedGroups,unusedGroups(ind));
end
H = zeros(size(W,2),1);
H(ismember(p.groups,usedGroups)) = tmp(:,ind);

end