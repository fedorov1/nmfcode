function [x] = ModeNNSBL(A,Y,p)
[d, m] = size(A);
lambda = p.lambda;
gamma = ones(m,1);

I = lambda*eye(d);

%Update A and gamma
for k = 1:m
    J(:,:,k) = A(:,k)*A(:,k)';
end
J_2d = reshape(J,d^2,m);

for iter = 1:1000
    G = diag(gamma);
    AG = A*G;
    AGAt = reshape(J_2d*gamma,d,d);
    Sigma = G - AG'*((I+AGAt)\AG);
    mu = Sigma*A'*Y/lambda;
    sigma = real(diag(Sigma));
    gamma_prev = gamma;
    gamma = max(mu.^2+sigma+mu.*sqrt(2/pi).*sqrt(sigma).*erfc(-mu.^2./(2*sigma))./erfc(-mu./(sqrt(2*sigma)+eps)),1e-8);
    if(norm(gamma-gamma_prev,2)/norm(gamma_prev) < 1e-6), break; end
end


x = lsqnonneg([A;diag(1./sqrt(gamma))*lambda],[Y;zeros(length(gamma),1)]);
end
