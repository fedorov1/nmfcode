load('./Results/rl2_cv.mat')
l = 6;
display(strcat('sparsity level = ',num2str(L(l))));
tmp = ps(1:1,:,:,l);
k = find(tmp == max(tmp(:)));
display(strcat('Best accuracy = ',num2str(max(tmp(:)))))
[ii,jj,kk] = ind2sub(size(squeeze(tmp)),k(1));
display(strcat('lambda = ',num2str(jj*lambda(ii))));
display(strcat('tau = ',num2str(tau(kk))))
%%
load('./Results/rl2_cv.mat')
l = 6;
display(strcat('sparsity level = ',num2str(L(l))));
tmp = ps(:,:,l);
k = find(tmp == max(tmp(:)));
display(strcat('Best accuracy = ',num2str(max(tmp(:)))))
[ii,jj] = ind2sub(size(squeeze(tmp)),k(1));
display(strcat('lambda = ',num2str(jj*lambda(ii))));