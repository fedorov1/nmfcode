function [success] = DictionaryQuality(D,Dhat)

success = zeros(size(D,2),1);
Dhat_norm = sqrt(sum(Dhat.^2,1));
for ii = 1:size(D,2)
    success(ii) = min(1-abs(D(:,ii)'*Dhat)./(norm(D(:,ii))*Dhat_norm));
end

end