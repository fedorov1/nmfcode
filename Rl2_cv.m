function Rl2_cv()
load('nmf_experiment_data.mat');
lambda = [1:9]*1e-5;
p.tau = 1;
p.sparseType = 'rl2';

W = W{1,2};
X = X{1,2,6};
H = H{1,2,6};
L = L;

parfor ii = 1:length(lambda)
    p1 = p;
    p1.lambda = lambda(ii);
    Hhat = MSC_GPU(X,W,ones(size(W,2),size(X,2)),p1);
    [~,ps(ii)] = CSdistortion(H,ThresholdMatrix(Hhat,L(6)));
end
save('./Results/rl2_cv2.mat');

