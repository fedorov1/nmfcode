function BaselineRl1()
load('nmf_experiment_data.mat');
p.sparseType = 'rl1';
p.lambda = 1e-4;
p.tau = 1;
L = L;
progressbar(0);

for iter = 1:size(X,1)
    for k = 1:size(X,2)
        W1 = W{iter,k};
        progressbar(((iter-1)*size(X,2)+(k-1))/(size(X,1)*size(X,2)));
        for l = 1:length(L)
            if((k == 1) || (k == 3))
                if(l ~= 6)
                    continue;
                end
            end
            Hhat{iter,k,l} = MSC_GPU(X{iter,k,l},W1,ones(size(W1,2),size(X{iter,k,l},2)),p);
            [l2_error(iter,k,l),ps(iter,k,l)] = CSdistortion(H{iter,k,l},Hhat{iter,k,l});
            save('./Results/rl1.mat');
        end
    end
end
end