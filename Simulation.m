function [L2error,support_dist] = Simulation(p)
rseed	= 1;
rand('state',rseed);
randn('state',rseed);
normrnd('state',rseed);

for ii = 1:length(p.s)
    s = p.s(ii);
    for iter = 1:p.N
        A = randn(p.m,p.n);
        
        if(p.nonneg)
            A = abs(A);
        end
        
        switch p.normalizeW
            case 1
                A = A*diag(1./sum(abs(A),1));
            case 2
                A = A*diag(1./sqrt(sum(abs(A).^2,1)));
        end
        
        Is = randperm(p.n,s);
        entries = normrnd(0,1,s,1);
        
        if(p.nonneg)
            entries = abs(entries);
        end
        
        x = zeros(p.n,1);
        x(Is) = entries;
        
        switch p.normalizeH
            case 1
                x = x/norm(x,1);
            case 2
                x = x/norm(x,2);
        end
        
        switch p.noiseType
            case 'Measurement'
                b = A*x;
                if(p.sigmaSquared > 0)
                    b = b + normrnd(0,sqrt(p.sigmaSquared),size(b));
                end
            case 'Signal'
                if(p.sigmaSquared > 0)
                    x = x + normrnd(0,sqrt(p.sigmaSquared),size(x));
                end
                b = A*x;
        end
        
        if(p.sigmaSquared > 0)
            b = b + normrnd(0,sqrt(p.sigmaSquared),size(b));
        end
        
        p.xTrue = x;
%         OPTIONS		= SB2_UserOptions('iterations',100,...
%             'diagnosticLevel', 2,...
%             'monitor', 10);
%         %
%         SETTINGS	= SB2_ParameterSettings('NoiseStd',1e-3);
%         xp1 = SparseBayes('Gaussian',A,b,OPTIONS,SETTINGS);
        xp = p.sparseCoder(A,b,s,p);
%         xp = mt_CS(A,b,1e4,1e0,1e-8);
        
        %Measure error
        L2error(ii,iter) = norm(x - xp)^2/norm(x)^2;
        
        [~,ind] = sort(abs(xp),'descend');
        xp_new = zeros(size(xp));
        xp_new(ind(1:s)) = xp(ind(1:s));
        xp = xp_new;
        Is_hat = find(xp);
        
        %Measure distance between Is and Is_hat
        support_dist(ii,iter) = (max(length(Is),length(Is_hat))-length(intersect(Is,Is_hat)))/max(length(Is),length(Is_hat));
    end
end

display(['s = ' num2str(s) ' se = ' num2str(mean(support_dist(ii,:)))]);
end


