function H = MSC_RSBL(X,W,H,p)

p.sigmaSquared = 1e-8;
for n = 1:size(X,2)
    switch p.sparseType
        case 'rl1'
            [gamma] = Reweightedl1NNSBL(W,X(:,n),p);
        case 'rl2'
            [~,gamma] = ModeNNSBL(W,X(:,n),p);
    end
   H(:,n) = lsqnonneg([W;diag(1./sqrt(gamma))*sqrt(p.sigmaSquared)],[X(:,n);zeros(length(gamma),1)]);
end

end