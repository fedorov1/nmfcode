load('nmf_experiment_data.mat');
p.aTrue = A{1,1}; p.xTrue = X1{1,1,1};
p.n = K;
p.s2_initial = 1;
p.decayRate = 0.99;
p.s2_lowerbound = 1e-8;
p.numIter = 1000;
MAP_NMF(Y{1,1,1},p);