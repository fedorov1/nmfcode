close all; figure; hold all; w = 3; k = 2;
load('./Results/l0.mat'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/l1.mat'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/rl1.mat'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/rl2.mat'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
legend('l0','l1','rl1','rl2')