function H = MSC(X,W,H,p)

[~,N] = size(X);
[~,K] = size(W);

if(isempty(H))
    H = 10*abs(pinv(W)*X);
end

mu0 = p.opts_slep.mu;
p.opts_slep.epsilon = p.opts_slep.epsilon*ones(1,N);
se = [];

for iter = 1:p.opts_slep.maxIter
    d = X; mu = mu0*ones(1,N); p.opts_slep.mu = mu;
    delta = p.opts_slep.delta*ones(1,N); p.converge = zeros(1,N);
    
    Hprev = H;
    if(p.ALM)
        for ii = 1:10
            H(:,max(H,[],1) < eps) = Hprev(:,max(H,[],1) < eps);
            H0 = H;
            
            H = MSC_GPU(X,W,H,p);
            
            tmp = sum(abs(X-W*H).^2,1) < delta;
            d(:,tmp) = d(:,tmp) - (W*H(:,tmp)-X(:,tmp));
            delta(tmp) = delta(tmp)*0.1;
            p.opts_slep.mu(~tmp) = p.opts_slep.mu(~tmp)/2;
            p.converge = sum(abs(X-W*H).^2,1) < p.opts_slep.deltaStop;
            
            if(sum(p.converge) == length(p.converge))
                break;
            end
            
            J0 = sum(abs(X-W*H0).^2,1);
            J = sum(abs(X-W*H).^2,1);
            if(max(H(:)) > 1e-30 && max(abs(J -J0)/J0) < p.opts_slep.deltaStop)
                break;
            end
        end
    else
        H = MSC_GPU(X,W,H,p);
    end
    
    if(strcmp(p.sparseType,'rl2'))
        tmp = abs(sqrt(sum(Hprev.^2,1))-sqrt(sum(H.^2,1)))./sqrt(sum(Hprev.^2,1));
        p.opts_slep.epsilon(tmp < sqrt(p.opts_slep.epsilon)/100) = p.opts_slep.epsilon(tmp < sqrt(p.opts_slep.epsilon)/100)/10;
        if(max(p.opts_slep.epsilon) < p.opts_slep.max_epsilon)
            break;
        end
    end
end

end