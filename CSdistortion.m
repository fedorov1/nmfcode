function [l2_error,probability_success] = CSdistortion(H,Hhat)
l2_error = norm(Hhat-H,'fro')/norm(H,'fro');
probability_success =  100*mean(sum((Hhat>0) & (H>0),1)./sum(H>0,1));
end
