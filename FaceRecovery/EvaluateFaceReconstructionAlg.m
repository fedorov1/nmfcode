function [l2_error,accuracy] = EvaluateFaceReconstructionAlg(Hhat)

load('./face_reconstruction.mat');
trainingInd = trainingInd; testingInd = testingInd; labels = labels;
W = X(:,trainingInd);
X = X(:,testingInd);
N = length(testingInd);
classes = unique(labels);
for n = 1:size(Hhat,2)
    [residual,outputLabel(n,1)] = ResidualClassifier(X(:,n),W,Hhat(:,n),labels(trainingInd),classes);
    l2_error(n,1) = residual(classes==labels(testingInd(n)));
end
accuracy = sum(outputLabel == labels(testingInd))/length(testingInd);

end