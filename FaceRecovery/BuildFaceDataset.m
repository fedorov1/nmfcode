function [X,L] = BuildFaceDataset(p)

X = []; L = [];
subjects = dir(p.dir);
close all; progressbar(0);
left_out = 0;

for n = 3:length(subjects)
    progressbar(n/length(subjects));
    faces = dir([p.dir '/' subjects(n).name]);
    
    for m = 1:length(faces)
        ind = regexp(faces(m).name,'.pgm','once');
        if(~isempty(ind))
            x = double(imread([p.dir '/' subjects(n).name '/' faces(m).name]))/255;
            
            if(norm(x(:),2) == 0)
                left_out = left_out+1;
                continue;
            end
                        
            if(size(x,1) ~= 192)
                left_out = left_out+1;
                continue;
            end
            
            x = imresize(x,p.downsampleFactor);
            
            if(any(any(isnan(x))))
                keyboard
            end
            X = [X,x(:)];
            L = [L;n];
        end
    end
end

end