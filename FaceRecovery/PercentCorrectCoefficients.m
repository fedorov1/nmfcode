function r = PercentCorrectCoefficients(x,labels,c)

ind = find(labels == c);
non_zero = find(x);
r = length(intersect(ind,non_zero))/length(non_zero);

end