function [residual,outputLabel] = ResidualClassifier(b,W,h,labels_train,classes)

residual = zeros(length(classes),1);
for cc = 1:length(classes)
    [x] = lsqnonneg(W(:,(h>0) & (labels_train==classes(cc))),b);
    residual(cc,1) = norm(b-W(:,(h>0) & (labels_train==classes(cc)))*x)/norm(b);
end
[~,ind] = sort(residual(:,1),'ascend');
outputLabel = classes(ind(1));

end