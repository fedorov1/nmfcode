function FaceReconstructionExperiment(type)
%%
load('./face_reconstruction.mat');
trainingInd = trainingInd; testingInd = testingInd; labels = labels;
W = X(:,trainingInd);
X = X(:,testingInd);
N = length(testingInd);
switch type
    case 'l0'
        Hhat = sparseNNLS(X,W,W'*W,W'*X,30,size(W,2),[]);
        parfor n = 1:N
            l2_error(n,1) = norm(W*(Hhat(:,n).*(labels(trainingInd) == labels(testingInd(n))))-X(:,n))/norm(X(:,n));
        end
        save('./Results/FaceRecovery/l0.mat','Hhat','l2_error');
    case 'l1'
        p.tau = 0.1; p.lambda = 1e-2; p.sparseType='l1';
        Hhat = MSC_GPU(X,W,ones(size(W,2),N),p);
        parfor n = 1:N
            l2_error(n,1) = norm(W*(Hhat(:,n).*(labels(trainingInd) == labels(testingInd(n))))-X(:,n))/norm(X(:,n));
        end
        save('./Results/FaceRecovery/l1.mat','Hhat','l2_error');
    case 'rl1'
        p.tau = 0.1; p.lambda = 1e-2; p.sparseType='rl1';
        Hhat = MSC_GPU(X,W,ones(size(W,2),N),p);
        parfor n = 1:N
            l2_error(n,1) = norm(W*(Hhat(:,n).*(labels(trainingInd) == labels(testingInd(n))))-X(:,n))/norm(X(:,n));
        end
        save('./Results/FaceRecovery/rl1.mat','Hhat','l2_error');
    case 'rl2'
        p.tau = 1; p.lambda = 1e-3; p.sparseType='rl2';
        Hhat = MSC_GPU(X,W,ones(size(W,2),N),p);
        parfor n = 1:N
            l2_error(n,1) = norm(W*(Hhat(:,n).*(labels(trainingInd) == labels(testingInd(n))))-X(:,n))/norm(X(:,n));
        end
        save('./Results/FaceRecovery/rl2.mat','Hhat','l2_error');
    case 'rl1_t2'
        p.lambda = 1e-4;
        parfor n = 1:N
            Xhat(:,n) = ModeNNSBL_rl1(D,X(:,n),p);
            l2_error(n,1) = norm(D*(Xhat(:,n).*(labels(trainingInd) == labels(testingInd(n))))-X(:,n))/norm(X(:,n));
        end
        save('./Results/FaceRecovery/rl1_t2.mat','Xhat','l2_error');
    case 'rl2_t2'
        p.lambda = 1e-4;
        parfor n = 1:N
            Xhat(:,n) = ModeNNSBL(D,X(:,n),p);
            l2_error(n,1) = norm(D*(Xhat(:,n).*(labels(trainingInd) == labels(testingInd(n))))-X(:,n))/norm(X(:,n));
        end
        save('./Results/FaceRecovery/rl2_t2.mat','Xhat','l2_error');
end