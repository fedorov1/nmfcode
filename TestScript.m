function [] = TestScript()


opts_slep.init = 2;        % starting from a zero point

% Termination criterion
opts_slep.tFlag=5; 
opts_slep.tol = 1e-5;
opts_slep.maxIter=1000;   
opts_slep.nFlag=0;       
opts_slep.rFlag = 0;
opts_slep.rsL2 = 0;
opts_slep.lambda = 1e-4;
opts_slep.epsilon = 1;
p.opts_slep = opts_slep;

p.opts_rl2.p = 0;
p.opts_rl2.lambda = 1e-4;

p.m = 100; p.n = 400; p.N = 500; 
p.s = [1,10:10:50];

p.nonneg = 0; p.display = 0;

p.distribution = 'Normal';
p.normalizeW = 2;
p.normalizeH = 0;

p.epsilon = 1e-2;

p.noiseType = 'Measurement';
p.sigmaSquared = 1e-6;

p.delta = 1e-3;
p.deltaStop = 1e-5;
p.mu(1:50) = 80;

p.ALM = 1;
%%
p.sparseCoder = @MSC; p.sparseType = 'l1';
[L2_error_l1,pe_l1] = Simulation(p);
%%
p.Type = types{2}; p.l1method = 'slep';
[L2error_l1_slep,support_dist_l1_slep] = Simulation(p);
%%
p.Type = types{3};
[L2error_rl1,support_dist_rl1] = Simulation(p);
close all; plot(mean(support_dist_rl1,2));
%%
p.Type = types{4};
[L2error_rl2,support_dist_rl2] = Simulation(p);
%%

p.sparseCoder = @ModeNNSBLNew;
p.sigmaEstimate = 1;
p.sigmaEstimateType = 'naive';
[L2error_sbl_naive,support_dist_sbl_naive] = Simulation(p);
mean(support_dist_sbl_naive,2)

p.sigmaEstimateType = 'prior';
[L2error_sbl_prior,support_dist_sbl_prior] = Simulation(p);
mean(support_dist_sbl_prior,2)
%%
p.Type = types{6};
[L2error_t2rl1,support_dist_t2rl1] = Simulation(p);
%%
p.parallel = 1; p.lambda = p.sigmaSquared; p.N = 10;
% p.groups = 1:p.n;
% p.numMeasurements = 1; display(['Number of measurements = ' num2str(p.numMeasurements)]);
% p.groups = 1:400;
% p.Type = 'Variational';
% [L2_1,se_1] = SimulationMMV(p);

p.numMeasurements = 200; display(['Number of measurements = ' num2str(p.numMeasurements)]);
p.groups = 1:400;
p.Type = 'Variational';
[L2_5,se_5] = SimulationMMV(p);
%%
close all; figure; hold all; linewidth = 3;
plot(p.s,mean(L2error_rsNNLS,2),'LineWidth',linewidth);
plot(p.s,mean(L2error_l1_slep,2),'LineWidth',linewidth);
plot(p.s,mean(L2error_rl2,2),'LineWidth',linewidth);
plot(p.s,mean(L2error_rl1,2),'LineWidth',linewidth);
plot(p.s,mean(L2error_sbl,2),'LineWidth',linewidth);
plot(p.s,mean(L2error_t2rl1,2),'LineWidth',linewidth);
legend('rsNNLS','l1','reweighted l_2 (Type 1)','reweighted l_1 (Type 1)','reweighted l_2 (Type 2)','reweighted l_1 (Type 2)','location','Northwest');
xlabel('Cardinality'); ylabel('Average L2 error'); set(gca,'FontSize',15);
print('-dpng','-r0','D:\Box Sync\SparsityProject\Report1\Figures\Results3\vector_coding_l2.png');

figure; hold all;
plot(p.s,mean(support_dist_rsNNLS,2),'LineWidth',linewidth);
plot(p.s,mean(support_dist_l1_slep,2),'LineWidth',linewidth);
plot(p.s,mean(support_dist_rl2,2),'LineWidth',linewidth);
plot(p.s,mean(support_dist_rl1,2),'LineWidth',linewidth);
plot(p.s,mean(support_dist_sbl,2),'LineWidth',linewidth);
plot(p.s,mean(support_dist_t2rl1,2),'LineWidth',linewidth);
legend('rsNNLS','l1','reweighted l_2 (Type 1)','reweighted l_1 (Type 1)','reweighted l_2 (Type 2)','reweighted l_1 (Type 2)','location','Northwest');
xlabel('Cardinality'); ylabel('Average support error'); set(gca,'FontSize',15);
print('-dpng','-r0','D:\Box Sync\SparsityProject\Report1\Figures\Results3\vector_coding_se.png');
%%
figure; hold all; linewidth = 3;
plot(p.s,mean(se,2),'LineWidth',linewidth);
legend('Rboust SBL')
xlabel('Cardinality'); ylabel('Average support error'); set(gca,'FontSize',15);

end