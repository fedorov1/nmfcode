function BaselineBlockRl1()

load('./nmf_block_experiment_data.mat');
p.groups = groups;
p.lambda = 1e-4;
p.tau = 0.1;
p.sparseType = 'block rl1';
progressbar(0);

for iter = 1:5
    Wtmp = W{iter};
    for l = 1:12
        progressbar(((iter-1)*12+(l-1))/(5*12));
        Htmp{iter,l} = MSC_GPU(X{iter,l},W{iter},ones(size(W{iter},2),size(X{iter,l},2)),p);
        l2_error(iter,l) = norm(Htmp{iter,l}-H{iter,l},'fro')/norm(H{iter,l},'fro');
        save('./Results/Block/rl1.mat');
    end
end

end