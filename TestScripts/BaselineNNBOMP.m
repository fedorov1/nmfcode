function BaselineNNBOMP()

load('./nmf_block_experiment_data.mat');
p.groups = groups;
p.s = 12;
progressbar(0);
for iter = 1:5
    Wtmp = W{iter};
    for l = 1:12
        progressbar(((iter-1)*12+(l-1))/(5*12));
        Xtmp = X{iter,l}; p.s = l; clear tmp
        N = size(Xtmp,2);
        parfor n = 1:N
            tmp(:,n) = NNBOMP(Xtmp(:,n),Wtmp,p);
        end
        Htmp{iter,l} = tmp;
        l2_error_nnbomp(iter,l) = norm(Htmp{iter,l}-H{iter,l},'fro')/norm(H{iter,l},'fro');
        save('./Results/Block/NNBOMP.mat');
    end
end

end