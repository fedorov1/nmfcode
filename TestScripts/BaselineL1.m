function BaselineL1()
load('nmf_experiment_data.mat');
progressbar(0);
p.sparseType = 'l1';
p.lambda = 1e-4;
load('./Results/GoodResults/l1.mat');
for iter = 1:size(X,1)
    for k = 1:size(X,2)
        D = W{iter,k};
        progressbar(((iter-1)*size(X,2)+k)/(size(X,1)*size(X,2)));
        for l = 1:length(L)
            if((k == 1) || (k == 3))
                if(l ~= 6)
                    continue;
                end
            end
%             Hhat{iter,k,l} = MSC_GPU(X{iter,k,l},D,ones(size(D,2),size(X{iter,k,l},2)),p);
            [l2_error(iter,k,l),ps(iter,k,l)] = CSdistortion(H{iter,k,l},MSC_NMF(X{iter,k,l},W{iter,k},ThresholdMatrix(Hhat{iter,k,l},L(l))));
            save('./Results/l1.mat');
        end
    end
end