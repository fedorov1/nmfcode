function BaselineRl2()
load('nmf_experiment_data.mat');
p.sparseType = 'rl2';
p.lambda = 1e-5; p.tau = 1;
L = L;
progressbar(0);
load('./Results/GoodResults/rl2.mat');
for iter = 1:size(X,1)
    for k = 1:size(X,2)
        W1 = W{iter,k};
        progressbar(((iter-1)*size(X,2)+(k-1))/(size(X,1)*size(X,2)));
        for l = 1:length(L)
            if((k == 1) || (k == 3))
                if(l ~= 6)
                    continue;
                end
            end
            if(l == length(L))
                p.lambda = 2e-5;
            else
                p.lambda = 1.1e-5;
            end
%             Hhat{iter,k,l} = MSC_GPU(X{iter,k,l},W1,ones(size(W1,2),size(X{iter,k,l},2)),p);
            [l2_error(iter,k,l),ps(iter,k,l)] = CSdistortion(H{iter,k,l},MSC_NMF(X{iter,k,l},W1,ThresholdMatrix(Hhat{iter,k,l},L(l))));
            save('./Results/rl2.mat');
        end
    end
end
end
