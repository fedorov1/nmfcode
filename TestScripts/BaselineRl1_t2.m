function BaselineRl1_t2()
load('nmf_experiment_data.mat');
p.lambda = 1e-8;
progressbar(0);
L = L;
for iter = 1:size(X,1)
    progressbar((iter-1)/size(X,1));
    for k = 1:size(X,2)
        W1 = W{iter,k};
        for l = 6:length(L)
            if((k == 1) || (k == 3))
                if(l ~= 6)
                    continue;
                end
            end
            tmp = zeros(size(H{iter,k,l}));
            xtmp = X{iter,k,l};
            parfor n = 1:size(H{iter,k,l},2)
                tmp(:,n) = ModeNNSBL_rl1(W1,xtmp(:,n),p);
            end
            Hhat{iter,k,l} = tmp;
            [l2_error(iter,k,l),ps(iter,k,l)] = CSdistortion(H{iter,k,l},Hhat{iter,k,l});
            save('./Results/rl1_t2.mat');
        end
    end
end
