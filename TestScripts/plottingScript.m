close all; figure; hold all; w = 3; k = 2; iter = 1:5;
load('./Results/l0.mat','l2_error'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/l1.mat','l2_error'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/rl1.mat','l2_error'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/rl2.mat','l2_error'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/rl1_t2.mat','l2_error'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
load('./Results/rl2_t2.mat','l2_error'); plot(mean(squeeze(l2_error(:,k,:)),1),'LineWidth',w);
legend('l0','l1','reweighted l1','reweighted l2','reweighted l1 (T2)','reweighted l2 (T2)','location','best')
%%
close all; k = 2; clear y
load('./Results/l0.mat','l2_error'); y(:,1) = mean(squeeze(l2_error(:,k,:)),1)';
load('./Results/l1.mat','l2_error'); y(:,2) = mean(squeeze(l2_error(:,k,:)),1)';
load('./Results/rl2.mat','l2_error'); y(:,3) = mean(squeeze(l2_error(:,k,:)),1)';
load('./Results/rl1.mat','l2_error'); y(:,4) = mean(squeeze(l2_error(:,k,:)),1)';
bar(y);
set(gca,'XTickLabel',[1,10:10:50])
set(gca,'FontSize',15)
set(0,'defaulttextinterpreter','latex')
xlabel('Sparsity level')
ylabel('Relative $l_2$ error')
h = legend('$l_0$','$l_1$','Reweighted $l_2$','Reweighted $l_1$');
set(h,'Interpreter','latex')
set(h,'location','northwest')
% print('-dpng','../NMF_Paper/Figures/l2_error_d400')
%%
close all; clear y
load('./Results/l0.mat','l2_error'); y(:,1) = [mean(squeeze(l2_error(:,1,6)));mean(squeeze(l2_error(:,2,6))); mean(squeeze(l2_error(:,3,6)))];
load('./Results/l1.mat','l2_error'); y(:,2) = [mean(squeeze(l2_error(:,1,6)));mean(squeeze(l2_error(:,2,6))); mean(squeeze(l2_error(:,3,6)))];
load('./Results/rl2.mat','l2_error'); y(:,3) = [mean(squeeze(l2_error(:,1,6)));mean(squeeze(l2_error(:,2,6))); mean(squeeze(l2_error(:,3,6)))];
load('./Results/rl1.mat','l2_error'); y(:,4) = [mean(squeeze(l2_error(:,1,6)));mean(squeeze(l2_error(:,2,6))); mean(squeeze(l2_error(:,3,6)))];
bar(y);
set(gca,'XTickLabel',[200,400,800])
set(gca,'FontSize',15)
set(0,'defaulttextinterpreter','latex')
xlabel('Dictionary Size')
ylabel('Relative $l_2$ error')
h = legend('$l_0$','$l_1$','Reweighted $l_2$','Reweighted $l_1$');
set(h,'Interpreter','latex')
set(h,'location','northwest')
print('-dpng','../NMF_Paper/Figures/l2_error_d248')
%%
close all; clear y
load('./Results/Block/NNBOMP.mat'); y(:,1) = mean(l2_error_nnbomp(:,1:10),1)';
load('./Results/Block/rl2.mat','l2_error'); y(:,2) = mean(l2_error(:,1:10),1)';
load('./Results/Block/rl1.mat','l2_error'); y(:,3) = mean(l2_error(:,1:10),1)';
b = bar(y);
set(gca,'XTickLabel',[1:10])
set(gca,'FontSize',15)
set(0,'defaulttextinterpreter','latex')
xlabel('Number of non-zero blocks')
ylabel('Relative $l_2$ error')
h = legend('$l_0$','Block Reweighted $l_2$','Block Reweighted $l_1$');
set(h,'Interpreter','latex')
set(h,'location','northwest')
print('-dpng','../NMF_Paper/Figures/l2_error_block')
%%
close all; clear y
load('./Results/FaceRecovery/l0.mat'); y(:,1) = mean(l2_error);
load('./Results/FaceRecovery/l1.mat'); y(:,2) = mean(l2_error);
load('./Results/FaceRecovery/rl2.mat'); y(:,3) = mean(l2_error);
load('./Results/FaceRecovery/rl1.mat'); y(:,4) = mean(l2_error);

b = bar([y;y],0.4,'grouped');
xlim([0.5,1.5])
ylim([0.3,0.4]);
set(gca,'XTickLabel',[])
set(gca,'FontSize',15)
set(0,'defaulttextinterpreter','latex')
ylabel('Relative $l_2$ error')
h = legend('$l_0$','$l_1$','Reweighted $l_2$','Reweighted $l_1$');
set(h,'Interpreter','latex')
set(h,'location','northeast')
% print('-dpng','../NMF_Paper/Figures/face_recovery.png')