function Rl1_cv()
load('nmf_experiment_data.mat');
L = L;
progressbar(0);
lambda = [1e-6,1e-5,1e-4,1e-3,1e-2];
tau = [1:-0.1:0.1];
p.sparseType = 'rl1';

W = W{1,2};
l = 6;
X = X{1,2,6};
H = H{1,2,6};
for ii = 1:length(lambda)
    progressbar((ii-1)/length(lambda));
    p.lambda = lambda(ii);
    for t = 1:length(tau)
        p.tau = tau(t);
        Hhat = MSC_GPU(X,W,ones(size(W,2),size(X,2)),p);
        [~,ps(ii,t)] = CSdistortion(H,ThresholdMatrix(Hhat,50));
    end
    save('./Results/rl1_cv2.mat');
end
