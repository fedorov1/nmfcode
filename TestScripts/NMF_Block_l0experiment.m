function NMF_Block_l0experiment()
N = 100;
K = 160;
M = 80;
d = 8;

for ii = 1:K/d
    groups(1+(ii-1)*d:ii*d) = ii;
end

for iter = 1:5
    W{iter} = NormalizeMatrix(abs(randn(M,K)));
    for l = 1:12
        H{iter,l} = zeros(K,N);
        
        for n = 1:N
            Is = randperm(K/d,l);
            for nn = 1:length(Is)
                H{iter,l}((d*(Is(nn)-1)+1:d*Is(nn)),n) = abs(normrnd(0,1,d,1));
            end
        end
        H{iter,l} = NormalizeMatrix(H{iter,l});
        X{iter,l} = W{iter}*H{iter,l};
    end
end
save('nmf_block_experiment_data.mat');


