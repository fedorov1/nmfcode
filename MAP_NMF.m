function [A,mu] = MAP_NMF(Y,p)
s2 = p.s2_initial;
[d,N] = size(Y);
M = p.n;
gamma = ones(M,N);
A = NormalizeMatrix(Y(:,randperm(N,M)));
used_ind = [];
second_moment = zeros(M,N);
tmp = zeros(M,M);
off_diagonal = 1-eye(M);

for iter = 1:p.numIter
    display(num2str(iter/p.numIter));
    %{
    if iter > 2
        M = size(A,2);
        Anorm = sqrt(sum(A.^2,1));
        
        %Check for column alignment, assuming columns are normalized to 1
        for m = 1:M
            ip = abs(A(:,m)'*A./(norm(A(:,m))*Anorm)); ip(m) = 0;
            if(max(ip) > 0.99 || mean(abs(mu(m,:))) < 0.01)
                Error = Y-A(:,setdiff(1:M,m))*mu(setdiff(1:M,m),:);
                Energy = sum(Error.^2,1);
                [~,ind] = sort(Energy,'descend');
                
                for n = ind
                    if(sum(n == used_ind) == 0)
                        A(:,m) = Y(:,n)/norm(Y(:,n));
                        gamma(m,:) = 1;
                        
                        %                             I = s2*eye(d(1));
                        %                             I2 = s3*eye(d(2));
                        %
                        %                             %Update A and gamma
                        %                             parfor kk = 1:M
                        %                                 J1(:,:,kk) = A{1}(:,kk)*A{1}(:,kk)';
                        %                                 J2(:,:,kk) = A{2}(:,kk)*A{2}(:,kk)';
                        %                             end
                        %
                        %                             J1_2d = reshape(J1,d(1)^2,M);
                        %                             J2_2d = reshape(J2,d(2)^2,M);
                        %
                        %                             %Calculate sufficient statistics
                        %                             for nn = 1:N(1)
                        %                                 G = diag(gamma(:,nn));
                        %                                 AG1 = A{1}*G; AG2 = A{2}*G;
                        %                                 AGAt1 = reshape(J1_2d*gamma(:,nn),d(1),d(1)); AGAt2 = reshape(J2_2d*gamma(:,nn),d(2),d(2));
                        %
                        %                                 Sigma1(:,:,nn) = G - AG1'*((I+AGAt1)\AG1); Sigma2(:,:,nn) = G - AG2'*((I2+AGAt2)\AG2);
                        %                                 mu1(:,nn) = Sigma1(:,:,nn)*A{1}'*Y{1}(:,nn)/s2; mu2(:,nn) = Sigma2(:,:,nn)*A{2}'*Y{2}(:,nn)/s3;
                        %
                        %                                 gamma(m,nn) = (diag(Sigma1(m,m,nn))+mu1(m,nn).^2 + diag(Sigma2(m,m,nn))+mu2(m,nn).^2)/2;
                        %                             end
                        %                             mu{1} = mu1; mu{2} = mu2;
                        used_ind = [used_ind;n];
                        break;
                    end
                    Anorm = sqrt(sum(A.^2,1));
                end
            end
        end
    end
    %}
    I = s2*eye(d);
    
%     %Update A and gamma
%     parfor k = 1:M
%         J(:,:,k) = A(:,k)*A(:,k)';
%     end
%     
%     J_2d = reshape(J,d^2,M);
%     
%     %Calculate sufficient statistics
%     parfor n = 1:N
%         G = diag(gamma(:,n));
%         AG = A*G;
%         AGAt = reshape(J_2d*gamma(:,n),d,d);
%         
%         Sigma = G - AG'*((I+AGAt)\AG);
%         mu = Sigma*A'*Y(:,n)/s2;
%         
%         S = diag(Sigma); S_sqrt = sqrt(S);
%         
%         first_moment(:,n) = mu+sqrt(2/pi)*S_sqrt.*exp(-mu.^2./(2*S+eps))./(erfc(-mu./(sqrt(2)*S_sqrt+eps))+eps);
%         second_moment(:,n) = mu.^2+S+mu.*sqrt(2/pi).*S_sqrt.*exp(-mu.^2./(2*S+eps))./(erfc(-mu./(sqrt(2*S)+eps))+eps);
%         
%         R(:,:,n) = first_moment(:,n)*first_moment(:,n)';
%         
%         gamma(:,n) = max(second_moment(:,n),1e-8);
%     end
%     
%     if(any(second_moment(:) < 0)), keyboard; end
%     
%     for n = 1:N
%         R(:,:,n) = R(:,:,n).*off_diagonal+diag(second_moment(:,n));
%     end
%     
%     if(rcond(first_moment*first_moment'*sum(R,3)) < 1e-15), keyboard; end
%     A = NormalizeMatrix(Y*first_moment'/(first_moment*first_moment'*sum(R,3)));
    A = NormalizeMatrix(Y*p.xTrue'/(p.xTrue*p.xTrue'*p.xTrue*p.xTrue'));
    s2 = max(s2*p.decayRate,p.s2_lowerbound);
    
    dictQ(iter) = mean(DictionaryQuality(p.aTrue,A));
    plot(dictQ); drawnow;
end

end

function A = NormalizeMatrix(A)
A = A*diag(1./sqrt(sum(A.^2,1)));
end
